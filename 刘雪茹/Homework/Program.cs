﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入玩家身份:");
            string useName = Console.ReadLine();
            Console.WriteLine("请输入咒语:");
            string usePassword = Console.ReadLine();
            string other;
            bool result = Login(useName, usePassword, out other);
            Console.WriteLine("登陆结果{0}:", result);
            Console.WriteLine("登陆信息：{0}", other);
            Console.WriteLine();

            double gold = 1000;
            GoldWashing(ref gold);
            Console.WriteLine(gold);
        }

        /// <summary>
        /// 输入正确账号密码返回成功着陆，否则返回提示信息
        /// </summary>
        public static bool Login(string name, string password, out string other)
        {
            if (name == "Alice" && password == "123456")
            {
                other = "你已成功着陆奇幻岛";
                return true;
            }
            else if (name == "Alice")
            {
                other = "玩家身份有误，请新塑造新身份";
                return false;
            }
            else if (password == "123456")
            {
                other = "请尝试其它咒语开启大门";
                return false;
            }
            else
            {
                other = "你没有资格进入，请再等一万年";
                return false;
            }
        }

        public static void GoldWashing(ref double g)
        {
            g += 600;
        }
    }
}
