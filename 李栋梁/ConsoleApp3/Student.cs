﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
   public class Student
    {
        public void BaseClass(ref int[,] a)
        {
            for (int i = 0; i < a.GetLength(0); i++)
            {
                Console.WriteLine("第" + (i + 1) + "个同学的语数外成绩");
                for (int j = 0; j < a.GetLength(1); j++)
                {
                    Console.WriteLine(a[i,j]);
                }
                Console.WriteLine("---");
            }

        }
        public void Add(ref int[,] a)
        { int sum=0;
            for (int i = 0; i < a.GetLength(0); i++)
            {
                Console.WriteLine("第"+(i + 1) + "个同学总成绩为：");
                for (int j = 0; j <a.GetLength(1); j++)
                {
                    sum+= a[i,j];
                   
                }
                Console.WriteLine(sum);
                sum = 0;
            }
        }

       
    }
   

    public  class ChinkClass: Student
    {
     
        public string Name(out string name)
        {
            name = "My name:YanWang";
            return name;
        }
        private int year = 2020;
        public int Age(out int age)
        {
            age = 2001;
            age -= year ;
            return age;
        }
    }
}
