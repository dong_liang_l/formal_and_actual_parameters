﻿using System;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {

            int[,] a = new int[3,3]{ { 90, 91, 92 }, { 60, 61, 62 }, { 98, 99, 100 } };
            ChinkClass cc = new ChinkClass();
            cc.BaseClass(ref a);
            cc.Add(ref a);
            _ = cc.Name(out _);
            Console.WriteLine(cc.Name(out _));
            _ = cc.Age(out _);
            Console.WriteLine("我的年龄："+Math.Abs(cc.Age(out _)));
           
        }
    }
}
