﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp4
{
    public class Ss1
    {
        //行参/实参
        public int xcsc (int a,int b)
        {
            return a + b;
        }
        public void print()
        {
            Console.WriteLine(xcsc(1, 1));
        }

        //ref类型
        public bool ref1(ref int num)
        {
            if (num % 5 == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        //out类型
        public void out1(int num,out bool br)
        {
            if (num % 5 == 0)
            {
                br = true;
            }
            else
            {
                br = false;
            }

        }
    }
}
